import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import { LocationState, TimelinesState, StatsState } from './interfaces';
import * as reducers from './reducers';

export type RootState = {
  location: LocationState,
  timelines: TimelinesState,
  stats: StatsState,
};

const composeEnhancers = (window as any)['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;

export default createStore(combineReducers(reducers), {}, composeEnhancers(
  applyMiddleware(thunk)
));


