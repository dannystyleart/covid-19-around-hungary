import { Reducer } from 'redux';
import { LocationActionTypes, LocationState } from '../interfaces';

const reducer: Reducer<LocationState, LocationActionTypes> = (state = null, action) => {
  switch (action.type) {
    case '@location/set':
      return {
        ...state,
        country: action.data.country,
        landborders: action.data.landborders ? action.data.landborders : []
      }
    default:
      return state;
  }

};

export default reducer;