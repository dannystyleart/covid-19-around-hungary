import { Reducer } from "react";
import { TimelinesActionTypes, TimelinesState, TimelineRecords } from "../interfaces";

const stateWithTimelineData = (state: TimelinesState, countryId: string, records: TimelineRecords) => {
  const existingState = state?.[countryId] || {} as TimelineRecords;

  const writeData = (
    results: typeof existingState,
    key: keyof TimelineRecords,
  ) => ({
    ...results,
    [key]: {
      ...existingState[key],
      ...records[key],
    }
  })

  return {
    ...state,
    [countryId]: Object.keys(records).reduce(
      (countryTimeline, key) => writeData(countryTimeline, key as keyof TimelineRecords),
      existingState
    )
  };
};


const reducer: Reducer<
  TimelinesState,
  TimelinesActionTypes
> = (state = null, action) => {
  switch (action.type) {
    case '@timelines/store':
      return stateWithTimelineData(state, action.countryId, action.data);

    case '@timelines/store-batch':
      return action.data.reduce((timelines, [
        countryId,
        data
      ]) => stateWithTimelineData(timelines, countryId, data), state);

    default:
      return state;
  }
};

export default reducer;