import { Reducer } from "react";
import { StatsActionTypes, StatsState } from "../interfaces";

const reducer: Reducer<
  StatsState,
  StatsActionTypes
> = (state = null, action) => {
  switch (action.type) {
    case '@stats/store':
      return {
        ...state,
        [action.countryId]: {
          ...state?.[action.countryId],
          ...action.data,
        }
      };

    case '@stats/store-batch':
      return action.data.reduce((result, [countryId, data]) => ({
        ...result,
        [countryId]: {
          ...result?.[countryId],
          ...data,
        }
      }), state)

    default:
      return state;
  }
}

export default reducer;