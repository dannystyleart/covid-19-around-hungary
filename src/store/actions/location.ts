import { ThunkAction } from "redux-thunk";
import { RootState } from "..";
import { CountryCollectionAPI } from "../../services/countryCollectionApi";
import { LocationActionTypes } from "../interfaces";

export const setCurrentLocation =
  (
    country: CountryCollectionAPI.CountryNeighboursResponse["country"],
    landborders?: CountryCollectionAPI.CountryNeighboursResponse["neighbours"]
  ): ThunkAction<void, RootState, unknown, LocationActionTypes> =>
    (dispatch) => {
      dispatch({
        type: "@location/set",
        data: { country, landborders },
      });
    };
