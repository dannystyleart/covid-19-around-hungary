import { ThunkAction } from "redux-thunk";
import { RootState } from "..";
import { DiseaseShAPI } from "../../services/diseaseApi";
import { StatsActionTypes } from "../interfaces";

export const storeStats =
  (
    countryId: string,
    data: DiseaseShAPI.HistoricalCountryStatsResponse
  ): ThunkAction<void, RootState, unknown, StatsActionTypes> =>
    (dispatch) => {
      dispatch({
        type: "@stats/store",
        countryId,
        data,
      });
    };

export const storeStatsBatch =
  (
    data: Array<
      [countrId: string, stats: DiseaseShAPI.HistoricalCountryStatsResponse]
    >
  ): ThunkAction<void, RootState, unknown, StatsActionTypes> =>
    (dispatch) => {
      dispatch({
        type: "@stats/store-batch",
        data,
      });
    };
