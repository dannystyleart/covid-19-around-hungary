import { ThunkAction } from "redux-thunk";
import { RootState } from "..";
import { TimelineRecords, TimelinesActionTypes } from "../interfaces";

export const storeTimelinesBatch =
  (
    data: Array<[countryId: string, data: TimelineRecords]>,
  ): ThunkAction<void, RootState, unknown, TimelinesActionTypes> =>
    (dispatch) => {
      dispatch({
        type: "@timelines/store-batch",
        data,
      });
    };
