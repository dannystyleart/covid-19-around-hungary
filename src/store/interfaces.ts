import { CountryCollectionAPI } from "../services/countryCollectionApi";
import { DiseaseShAPI } from "../services/diseaseApi";

export type LocationState = {
  country: CountryCollectionAPI.CountryNeighboursResponse['country'];
  landborders: CountryCollectionAPI.CountryNeighboursResponse['neighbours']
} | null;

export type LocationActionTypes =
  {
    type: '@location/set',
    data: { country: CountryCollectionAPI.CountryNeighboursResponse['country'], landborders?: CountryCollectionAPI.CountryNeighboursResponse['neighbours'] }
  }

export type TimelinesState = {
  [countryId: string]: TimelineRecords;
} | null;

export type TimelineRecords = {
  cases: {
    [date: string]: number;
  },
  deaths: {
    [date: string]: number;
  },
  recovered: {
    [date: string]: number;
  }
};

export type TimelinesActionTypes = {
  type: '@timelines/store',
  countryId: string,
  data: TimelineRecords,
} | {
  type: '@timelines/store-batch',
  data: Array<[countryId: string, data: TimelineRecords]>
}


export type StatsState = {
  [countryId: string]: DiseaseShAPI.HistoricalCountryStatsResponse,
} | null;
export type StatsActionTypes = {
  type: '@stats/store',
  countryId: string,
  data: DiseaseShAPI.HistoricalCountryStatsResponse
} | {
  type: '@stats/store-batch',
  data: Array<[countryId: string, data: DiseaseShAPI.HistoricalCountryStatsResponse]>
};

export type AppActionTypes = LocationActionTypes & TimelinesActionTypes & StatsActionTypes;

export type RootState = {
  location: LocationState,
  timelines: TimelinesState,
  stats: StatsState,
};