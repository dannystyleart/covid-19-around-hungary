const getComponentDisplayName = (Component: React.ComponentType<any>): string | 'Component' => Component.displayName || Component.name || "Component";
export default getComponentDisplayName;