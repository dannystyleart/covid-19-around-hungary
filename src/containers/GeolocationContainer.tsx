import { useEffect, useMemo, useRef } from "react";
import { connect, MapDispatchToProps, MapStateToProps } from "react-redux";
import { bindActionCreators } from "redux";
import { ThunkActionDispatch } from "redux-thunk";
import createCountryCollectionClient, {
  CountryCollectionAPI,
} from "../services/countryCollectionApi";
import {
  fetchReverseGeolocation,
  getDeviceLocation,
} from "../services/geolocationApi";
import { RootState } from "../store";
import * as actions from "../store/actions";
import { noop } from "../utils/noop";

type GeolocationContainerProps = {
  onLocationSet?: (location: CountryCollectionAPI.CountryDTO) => void;
};
type MappedState = Pick<RootState, "location">;
type MappedActions = {
  setCurrentLocation: ThunkActionDispatch<typeof actions.setCurrentLocation>;
};

type PropTypes = MappedState & MappedActions & GeolocationContainerProps;

const mapStateToProps: MapStateToProps<
  MappedState,
  GeolocationContainerProps,
  RootState
> = ({ location }) => ({ location });

const mapDispatchToProps: MapDispatchToProps<
  MappedActions,
  GeolocationContainerProps
> = (dispatch: any) =>
  bindActionCreators(
    {
      setCurrentLocation: actions.setCurrentLocation,
    },
    dispatch
  );

const GeolocationContainer: React.FC<PropTypes> = ({
  setCurrentLocation,
  onLocationSet,
  location,
}) => {
  const locateRequest = useRef<Promise<void> | null>(null);
  const countryClient = useMemo(
    () =>
      createCountryCollectionClient({
        baseURL: "https://country-collection.herokuapp.com/api",
      }),
    []
  );

  useEffect(() => {
    if (location?.country || locateRequest.current) return noop();

    locateRequest.current = getDeviceLocation()
      .then(({ coords: { latitude, longitude } }) =>
        fetchReverseGeolocation(latitude, longitude)
      )
      .then(({ address: { country_code } }) =>
        countryClient.neighbours(country_code)
      )
      .then(({ country, neighbours }) => {
        setCurrentLocation(country, neighbours);
        onLocationSet?.(country);

        locateRequest.current = null;
      });
  }, [countryClient, location, setCurrentLocation, onLocationSet]);

  return null;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GeolocationContainer);
