import { useEffect, useMemo, useRef, useState } from "react";
import { connect, MapDispatchToProps, MapStateToProps } from "react-redux";
import { bindActionCreators } from "redux";
import { ThunkActionDispatch } from "redux-thunk";
import { ChartComponent } from "../components/chart";
import { CountryCard } from "../components/countryCard";
import { api } from "../services/diseaseApi";
import * as actions from "../store/actions";
import { RootState } from "../store/interfaces";
import { noop } from "../utils/noop";
import "./AppContainer.scss";
import GeolocationContainer from "./GeolocationContainer";

type DispatchedActions = {
  storeTimelinesBatch: ThunkActionDispatch<typeof actions.storeTimelinesBatch>;
  storeStatsBatch: ThunkActionDispatch<typeof actions.storeStatsBatch>;
};

type Props = Pick<RootState, "location" | "stats" | "timelines"> &
  DispatchedActions;

const App: React.FC<never> = ({
  storeTimelinesBatch,
  storeStatsBatch,
  location,
  stats,
  timelines,
}: Props) => {
  const initReq = useRef<any>(null);
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (initialized || !location || initReq.current) return noop();
    const { country, landborders } = location;
    const countryIds = [country, ...landborders].map(({ iso3 }) => iso3);

    const statsRequest = api.countryBatch(countryIds).then((responses) => {
      const statsData = responses.map((response, index) => [
        countryIds[index],
        response,
      ]) as any;

      storeStatsBatch(statsData);
    });

    const historyRequest = api
      .countryHistoryBatch(countryIds, 21)
      .then((responses) => {
        const timelineData = responses.map((response, index) => [
          countryIds[index],
          response,
        ]) as any;

        storeTimelinesBatch(timelineData);
      });

    initReq.current = Promise.allSettled([statsRequest, historyRequest]).then(
      () => {
        setInitialized(true);
      }
    );
  }, [
    location,
    setInitialized,
    initialized,
    initReq,
    storeStatsBatch,
    storeTimelinesBatch,
  ]);

  // TODO: use reselect selector for these
  const borderCountries = useMemo(() => {
    if (!initialized) return [];

    return location?.landborders.map(({ iso3 }) => ({
      key: iso3,
      stats: stats?.[iso3],
    }));
  }, [location, stats, initialized]);

  // TODO: use reselect selector for these
  const stagedCountryHistory = useMemo(() => {
    if (!location?.country || !timelines) return undefined;

    return timelines[location.country.iso3 as keyof typeof timelines] as any;
  }, [timelines, location]);

  const stagetCountryStats = useMemo(() => {
    if (!location?.country || !stats) return undefined;

    return stats[location.country.iso3 as keyof typeof stats];
  }, [stats, location]);

  return (
    <>
      <GeolocationContainer onLocationSet={() => setInitialized(true)} />
      <header className="hero">
        <div className="wrapper">
          <div className="item-grid">
            <CountryCard staged stats={stagetCountryStats} />
            <ChartComponent history={stagedCountryHistory} />
          </div>
        </div>
      </header>
      <section className="wrapper">
        <div className="item-grid">
          {borderCountries?.map((countryProps) => (
            <CountryCard className="item-card" {...countryProps} />
          ))}
        </div>
      </section>
    </>
  );
};

const mapStateToProps: MapStateToProps<
  Pick<RootState, "location" | "stats" | "timelines">,
  any,
  RootState
> = ({ location, stats, timelines }) => ({
  location,
  stats,
  timelines,
});

const mapDispatchToProps: MapDispatchToProps<DispatchedActions, any> = (
  dispatch
) =>
  bindActionCreators(
    {
      storeTimelinesBatch: actions.storeTimelinesBatch,
      storeStatsBatch: actions.storeStatsBatch,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
