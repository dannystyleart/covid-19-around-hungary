import { request } from "./request";

export class GeolocationException extends Error {
  name = 'GeoLocationException'
};

export enum DeviceLocationError {
  API_NOT_AVAILABLE = 'Method not available',
  PERMISSION_DENIED = 'Denied by user',
  POSITION_NOT_AVAILABLE = 'Position not available',
  TIMEOUT = 'Timeout',
}

export const getDeviceLocation = (): Promise<GeolocationPosition> => new Promise((resolve, reject) => {
  if (!navigator?.geolocation?.getCurrentPosition) return reject(
    new GeolocationException(DeviceLocationError.API_NOT_AVAILABLE)
  );

  const handleError = (error: GeolocationPositionError) => {
    const { code, message } = error;
    let exceptionMessage: DeviceLocationError | string;

    switch (code) {
      case error.PERMISSION_DENIED:
        exceptionMessage = DeviceLocationError.PERMISSION_DENIED;
        break;

      case error.POSITION_UNAVAILABLE:
        exceptionMessage = DeviceLocationError.POSITION_NOT_AVAILABLE;
        break;

      case error.TIMEOUT:
        exceptionMessage = DeviceLocationError.TIMEOUT;
        break;

      default:
        exceptionMessage = message;
        break;
    };

    reject(new GeolocationException(exceptionMessage));
  };

  navigator.geolocation.getCurrentPosition(resolve, handleError)
});

export const ReverGeolocationApiError = 'Niminatim API Error'

export interface NominatimApiResponse {
  display_name: string,
  address: {
    country: string,
    country_code: string,
  }
}
export const fetchReverseGeolocation = (lat: number, lon: number) => request.get<NominatimApiResponse>(
  'https://nominatim.openstreetmap.org/reverse',
  { params: { lat, lon, format: 'json' }, mode: 'cors' }
).catch(() => {
  throw new GeolocationException(ReverGeolocationApiError)
});