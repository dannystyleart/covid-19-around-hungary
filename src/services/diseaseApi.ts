import { request } from "./request";

export namespace DiseaseShAPI {
  export interface CountryDTO {
    _id: string;
    flag: string;
    iso2: string;
    iso3: string;
    lat: number;
    long: number;
  }

  export interface RecentCountryStatsResponse {
    countryInfo: CountryDTO;
    updated: number;
    country: string;
    cases: number;
    todayCases: number;
    deaths: number;
    todayDeaths: number;
    recovered: number;
    todayRecovered: number;
    active: number;
    critical: number;
    casesPerOneMillion: number;
    deathsPerOneMillion: number;
    tests: number;
    testsPerOneMillion: number;
    population: number;
    continent: string;
    oneCasePerPeople: number;
    oneDeathPerPeople: number;
    oneTestPerPeople: number;
    activePerOneMillion: number;
    recoveredPerOneMillion: number;
    criticalPerOneMillion: number;
  }
  export type RecentCountryStatsBatchResponse =
    Array<RecentCountryStatsResponse>;

  export type HistoricalTimelineRecords = { [date: string]: number };
  export type HistoricalStats = {
    cases: HistoricalTimelineRecords;
    deaths: HistoricalTimelineRecords;
    recovered: HistoricalTimelineRecords;
  };

  export interface HistoricalCountryStatsResponse {
    country: string;
    province?: string[];
    timeline: HistoricalStats;
  }
  export type HistoricalCountryStatsBatchResponse =
    Array<HistoricalCountryStatsResponse>;
}

const diseaseApi = request.create({
  baseURL: "https://disease.sh/v3/covid-19",
  forceCors: true,
});

export const api = {
  country: (countryId: string) =>
    diseaseApi.get<DiseaseShAPI.RecentCountryStatsResponse>(`/countries/${countryId}`),

  countryBatch: (countryIds: string[]) =>
    diseaseApi.get<DiseaseShAPI.RecentCountryStatsBatchResponse>(
      `/countries/${countryIds.join(",")}`
    ),

  countryHistory: (countryId: string, lastDays: number | "all") =>
    diseaseApi.get<DiseaseShAPI.HistoricalCountryStatsResponse>(
      `/historical/${countryId}`,
      {
        params: { lastdays: lastDays },
      }
    ),

  countryHistoryBatch: (countryIds: string[], lastDays: number | "all") =>
    diseaseApi.get<DiseaseShAPI.HistoricalCountryStatsBatchResponse>(
      `/historical/${countryIds.join(",")}`,
      {
        params: { lastdays: lastDays },
      }
    ),
};
