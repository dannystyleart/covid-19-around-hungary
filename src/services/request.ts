type RequestConfig = Partial<RequestInit & {
  params: any,
}>

const buildUrl = (path: string, opts?: RequestConfig) => {
  const { params } = opts || {}
  const url = new URL(path);

  if (!!params) {
    Object.entries(params).forEach(
      ([key, value]) => url.searchParams.append(key, `${value}`)
    );
  }

  return url.toString();
};

const buildConfig = (opts?: RequestConfig) => {
  const {
    method = 'get',
    headers
  } = opts || {};

  return {
    method,
    headers: {
      accept: 'application/json',
      ...headers
    }
  }
};

const sendRequest = <T>(
  path: string,
  opts?: RequestConfig,
): Promise<T> => fetch(
  buildUrl(path, opts),
  buildConfig(opts)  ,
).then((r) => r.json());

type RequestCreateOptions = Partial<{
  baseURL: string,
  forceCors: boolean
}>

const createInstance = (opts?: RequestCreateOptions) => {
  const { baseURL, forceCors } = opts || {}

  const _urlBase = baseURL && baseURL.replace(/(\/+)$/gm, '');
  const withBaseUrl = (path: string) => [
    _urlBase,
    path.replace(/^(\/+)/gm, '')
  ].filter(Boolean).join('/');

  const enhanceOptions = (opts?: RequestConfig) => ({
    ...opts,
    ...(forceCors && { mode: 'cors' as RequestMode }),
  })

  return {
    get: <R>(path: string, opts?: RequestConfig) => sendRequest<R>(
      withBaseUrl(path),
      enhanceOptions(opts),
    ),

    post: <R>(path: string, opts?: RequestConfig) => sendRequest<R>(
      withBaseUrl(path),
      enhanceOptions({ ...opts, method: 'post' }),
    ),

    put: <R>(path: string, opts?: RequestConfig) => sendRequest<R>(
      withBaseUrl(path),
      enhanceOptions({ ...opts, method: 'put' }),
    ),

    patch: <R>(path: string, opts?: RequestConfig) => sendRequest<R>(
      withBaseUrl(path),
      enhanceOptions({ ...opts, method: 'patch' }),
    ),

    delete: <R>(path: string, opts?: RequestConfig) => sendRequest<R>(
      withBaseUrl(path),
      enhanceOptions({ ...opts, method: 'delete' }),
    ),
  }
};

export const request: ReturnType<typeof createInstance> & { create: typeof createInstance } = {
  ...createInstance(),
  create: createInstance,
};