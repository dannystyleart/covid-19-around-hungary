import { request } from './request';

export namespace CountryCollectionAPI {
  export interface CountryDTO {
    name: string;
    iso2: string;
    iso3: string;
  }

  export type CountryListResponse = Array<CountryDTO>;
  export type CountryFilterResponse = Array<CountryDTO>;
  export type CountryNeighboursResponse = {
    country: CountryDTO;
    neighbours: Array<CountryDTO>
  };
}

const factory = (opts: Parameters<typeof request.create>[0]) => {
  const resource = request.create(opts);

  return {
    list: () => resource.get<CountryCollectionAPI.CountryListResponse>('/countries'),
    find: (search: string) => resource.get<CountryCollectionAPI.CountryFilterResponse>(`/countries/${search}`),
    neighbours: (search: string) => resource.get<CountryCollectionAPI.CountryNeighboursResponse>(`/countries/${search}/neighbours`),
  }
};

export default factory;
