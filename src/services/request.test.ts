import fetchMock from "jest-fetch-mock";
import { request } from "./request";

describe("request", () => {
  const testUrl = "http://example.com";
  const testParams = { foo: 'bar' };

  beforeEach(() => {
    fetchMock.resetMocks();
    fetchMock.mockResponse(JSON.stringify(null));
  });

  test.each([
    ['get'],
    ['post'],
    ['put'],
    ['patch'],
    ['delete'],
  ])(
    '%s(url, config) method should be defined correctly and initiate a http request with identical method', async (methodName) => {
      const method = request[methodName as keyof typeof request];

      const expectedUrl = `${testUrl}/`;
      const expectedUrlWithParams = `${expectedUrl}?foo=bar`;
      const expectedConfig = expect.objectContaining({
        method: methodName,
        headers: expect.objectContaining({
          accept: "application/json",
        }),
      });

      expect(method).toBeDefined();
      expect(method).toHaveLength(2);

      await method(testUrl);
      expect(fetchMock).toHaveBeenCalledWith(
        expectedUrl,
        expectedConfig
      );

      fetchMock.mockClear();
      await method(testUrl, { params: testParams });
      expect(fetchMock).toHaveBeenCalledWith(
        expectedUrlWithParams,
        expectedConfig
      );
    });

  test('create method should create a request instance with options for all request methods', () => {
    expect(
      request.create({
        baseURL: 'http://hello.api',
      })
    ).toEqual(expect.objectContaining({
      get: expect.anything(),
      post: expect.anything(),
      put: expect.anything(),
      patch: expect.anything(),
      delete: expect.anything(),
    }))
  });

  describe('instance methods', () => {
    const testBaseURL = 'http://hello.api';
    const testEndpointPath = '/books';
    let instance: ReturnType<typeof request.create>;

    beforeAll(() => {
      instance = request.create({
        baseURL: 'http://hello.api',
        forceCors: true,
      });
    });

    test.each([
      ['get'],
      ['post'],
      ['put'],
      ['patch'],
      ['delete'],
    ])(
      '%s(url, config) method should be defined correctly and initiate a http request with identical method', async (methodName) => {
        const method = instance[methodName as keyof typeof instance];

        const expectedUrl = `${testBaseURL}${testEndpointPath}`;
        const expectedUrlWithParams = `${expectedUrl}?foo=bar`;
        const expectedConfig = expect.objectContaining({
          method: methodName,
          headers: expect.objectContaining({
            accept: "application/json",
          }),
        });

        expect(method).toBeDefined();
        expect(method).toHaveLength(2);

        await method(testEndpointPath);
        expect(fetchMock).toHaveBeenCalledWith(
          expectedUrl,
          expectedConfig
        );

        fetchMock.mockClear();
        await method(testEndpointPath, { params: testParams });
        expect(fetchMock).toHaveBeenCalledWith(
          expectedUrlWithParams,
          expectedConfig
        );
      });
  });
});
