import fetchMock from "jest-fetch-mock";
import { InteractionObject, Matchers } from "@pact-foundation/pact";
import { HTTPMethod } from "@pact-foundation/pact/src/common/request";
import { pactWith } from "jest-pact";
import createApi from "./countryCollectionApi";

fetchMock.disableMocks();

pactWith(
  {
    consumer: "CovidAroundUsApp",
    provider: "CountryDataApi",
  },
  (provider) => {
    describe("CountryCollectionApi", () => {
      let client: ReturnType<typeof createApi>;

      const mockCountryListItemData = {
        name: "Foo country",
        iso2: "FC",
        iso3: "FOO",
      };
      const mockCountryListResponseData = [mockCountryListItemData];

      const countryItemMatcher = {
        name: Matchers.somethingLike("Foo country"),
        iso2: Matchers.regex({
          matcher: "^([A-Z]{2})$",
          generate: "FC",
        }),
        iso3: Matchers.regex({
          matcher: "^([A-Z]{3})$",
          generate: "FOO",
        }),
      };

      const countryItemListMatcher = Matchers.eachLike(countryItemMatcher, {
        min: 1,
      });

      beforeEach(() => {
        client = createApi({
          baseURL: provider.mockService.baseUrl,
        });
      });

      describe("list method - for listing countries", () => {
        test("should be defined and accepts 0 parameter", () => {
          expect(client.list).toBeDefined();
          expect(client.list).toHaveLength(0);
        });

        describe("Listing countries", () => {
          test("should return all countries", async () => {
            const interaction: InteractionObject = {
              uponReceiving: "Listing countries",
              state: "All countries returned",
              withRequest: {
                method: HTTPMethod.GET,
                path: "/countries",
                headers: {
                  Accept: Matchers.somethingLike("application/json"),
                },
              },
              willRespondWith: {
                status: 200,
                headers: {
                  "Content-Type": Matchers.string("application/json"),
                },
                body: countryItemListMatcher,
              },
            };

            await provider.addInteraction(interaction);

            const response = await client.list();

            expect(response).toEqual(mockCountryListResponseData);
          });
        });
      });

      describe("find method - for search countries", () => {
        test("should be defined and accept a search term of a country", () => {
          expect(client.find).toBeDefined();
          expect(client.find).toHaveLength(1);
        });

        describe("Searching for a country", () => {
          const searchTerm = "Foo countr";

          test("should return search results when have", async () => {
            const interaction: InteractionObject = {
              uponReceiving: "Searching countries",
              state: "Countries found",
              withRequest: {
                method: HTTPMethod.GET,
                path: `/countries/${encodeURI(searchTerm)}`,
                headers: {
                  Accept: Matchers.somethingLike("application/json"),
                },
              },
              willRespondWith: {
                status: 200,
                headers: {
                  "Content-Type": Matchers.string("application/json"),
                },
                body: countryItemListMatcher,
              },
            };

            await provider.addInteraction(interaction);

            const response = await client.find(searchTerm);

            expect(response).toEqual(mockCountryListResponseData);
          });

          test("should return proper not found response", async () => {
            const interaction: InteractionObject = {
              uponReceiving: "Searching countries",
              state: "No country found",
              withRequest: {
                method: HTTPMethod.GET,
                path: `/countries/${encodeURI(searchTerm)}`,
                headers: {
                  Accept: Matchers.somethingLike("application/json"),
                },
              },
              willRespondWith: {
                status: 404,
                headers: {
                  "Content-Type": Matchers.string("application/json"),
                },
                body: {
                  error: Matchers.somethingLike("Not Found"),
                  message: Matchers.somethingLike(
                    `Could not find country for: ${searchTerm}`
                  ),
                  statusCode: Matchers.integer(404),
                },
              },
            };

            await provider.addInteraction(interaction);

            const response = await client.find(searchTerm);

            expect(response).toEqual(
              expect.objectContaining({
                error: expect.stringMatching("Not Found"),
                message: expect.stringMatching(
                  `Could not find country for: ${searchTerm}`
                ),
                statusCode: 404,
              })
            );
          });
        });
      });

      describe("neighbours method - for finding a country and its neighbours", () => {
        test("should be defined and accept a search term of a country", () => {
          expect(client.neighbours).toBeDefined();
          expect(client.neighbours).toHaveLength(1);
        });

        describe("Getting neighbour list of a country", () => {
          const searchTerm = "Foo countr";

          const neighbourRequest = {
            uponReceiving: "Getting neighbours of a country",
            withRequest: {
              method: HTTPMethod.GET,
              path: `/countries/${encodeURI(searchTerm)}/neighbours`,
              headers: {
                Accept: Matchers.somethingLike("application/json"),
              },
            },
          };

          test("should return country and its neighbours correctly", async () => {
            const interaction: InteractionObject = {
              ...neighbourRequest,
              state: "Country found that is having neighbours",
              willRespondWith: {
                status: 200,
                headers: {
                  "Content-Type": Matchers.string("application/json"),
                },
                body: {
                  country: countryItemMatcher,
                  neighbours: countryItemListMatcher,
                },
              },
            };

            await provider.addInteraction(interaction);

            const response = await client.neighbours(searchTerm);

            expect(response).toEqual(
              expect.objectContaining({
                country: mockCountryListItemData,
                neighbours: [mockCountryListItemData],
              })
            );
          });

          test("should return country and empty neighbours list correctly", async () => {
            const interaction: InteractionObject = {
              ...neighbourRequest,
              state: "Country found that has no neighbours",
              willRespondWith: {
                status: 200,
                headers: {
                  "Content-Type": Matchers.string("application/json"),
                },
                body: {
                  country: countryItemMatcher,
                  neighbours: [],
                },
              },
            };

            await provider.addInteraction(interaction);

            const response = await client.neighbours(searchTerm);

            expect(response).toEqual(
              expect.objectContaining({
                country: mockCountryListItemData,
                neighbours: [],
              })
            );
          });

          test("should return proper not found response", async () => {
            const interaction = {
              ...neighbourRequest,
              state: "Country not found",
              willRespondWith: {
                status: 404,
                headers: {
                  "Content-Type": Matchers.string("application/json"),
                },
                body: {
                  error: Matchers.somethingLike("Not Found"),
                  message: Matchers.somethingLike(
                    `Could not find country for: ${searchTerm}`
                  ),
                  statusCode: Matchers.integer(404),
                },
              },
            };

            await provider.addInteraction(interaction);

            const response = await client.neighbours(searchTerm);

            expect(response).toEqual(
              expect.objectContaining({
                error: expect.stringMatching("Not Found"),
                message: expect.stringMatching(
                  `Could not find country for: ${searchTerm}`
                ),
                statusCode: 404,
              })
            );
          });
        });
      });
    });
  }
);
