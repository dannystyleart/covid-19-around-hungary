import { FC, useMemo } from "react";
import { withDefault } from "../utils/withDetault";
import "./countryCard.scss";
import { CountryFlag } from "./countryFlag";
import { ShieldIcon } from "./shieldIcon";
import { StatField } from "./statField";

export type CountryCardProps = {
  staged?: boolean;
  className?: string;
  stats?: any;
  actions?: React.ElementType;
};

const withFallback = withDefault(0);

export const CountryCard: FC<CountryCardProps> = ({
  stats = {},
  staged,
  actions,
  className,
}) => {
  const updatedAt = useMemo(() => {
    if (!stats.updated) return "N/A";
    const dt = new Date(stats.updated);

    return dt.toUTCString();
  }, [stats]);

  const classNames = useMemo(
    () =>
      [className, "country-card", staged && "staged"].filter(Boolean).join(" "),
    [className, staged]
  );

  return (
    <div className={classNames}>
      <header>
        {stats?.countryInfo?.iso3 === "UKR" && (
          <ShieldIcon className="shield-icon" width={48} height={48} />
        )}

        <CountryFlag small={!staged} countryInfo={stats?.countryInfo} />
        <div className="country-heading">
          <span className="title">{stats?.country}</span>
          <span className="updated-time">{updatedAt}</span>
        </div>
        {actions && <div className="actions">{actions}</div>}
      </header>
      <div className="country-stats">
        <StatField
          staged={staged}
          label="Cases"
          value={withFallback(stats.todayCases)}
        />
        <StatField
          staged={staged}
          label="Active"
          value={withFallback(stats.active)}
        />
        <StatField
          staged={staged}
          label="Deaths"
          value={withFallback(stats.todayDeaths)}
        />
        <StatField
          staged={staged}
          label="Recovered"
          value={withFallback(stats.todayRecovered)}
        />
      </div>
    </div>
  );
};
