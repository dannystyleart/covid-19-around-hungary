import { FC, useMemo } from "react";
import { AnimatedCounter } from "./animatedCounter";
import "./statField.scss";

export type StatFieldProps = {
  staged?: boolean;
  label: string;
  value: any;
  duration?: number;
  animated?: boolean;
};

export const StatField: FC<StatFieldProps> = ({ staged, label, value }) => {
  const classNames = useMemo(
    () => ["field", staged && "large"].filter(Boolean).join(" "),
    [staged]
  );
  return (
    <div className={classNames}>
      <label>{label}</label>
      <AnimatedCounter end={value} />
    </div>
  );
};
