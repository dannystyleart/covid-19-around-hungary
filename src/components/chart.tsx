import { FC, useCallback, useMemo } from "react";
import { Chart } from "react-charts";
import { DiseaseShAPI } from "../services/diseaseApi";
import "./chart.scss";

const styleDefs = (
  <defs>
    <linearGradient id="gradient-danger" x1="0" x2="0" y1="1" y2="0">
      <stop offset="0%" stopColor="var(--gradient-danger-from, yellow)" />
      <stop offset="100%" stopColor="var(--gradient-danger-to, orange)" />
    </linearGradient>
  </defs>
);

type ChartProps = {
  history?: DiseaseShAPI.HistoricalCountryStatsResponse;
};

const toPairs = (input: { [k: string]: number }) =>
  Object.entries(input).map(([date, value]) => [date, value]);

export const ChartComponent: FC<ChartProps> = ({ history }) => {
  const data = useMemo(() => {
    let values: (string | number)[][] = [];

    if (history) {
      const { cases } = history?.timeline;
      values = toPairs(cases);
    }

    return [
      {
        label: "Total cases",
        data: values,
      },
    ];
  }, [history]);

  const series = useMemo(
    () => ({
      type: "area",
    }),
    []
  );

  const axes = useMemo(
    () => [
      {
        primary: true,
        type: "ordinal",
        position: "bottom",
        show: false,
      },
      { type: "linear", position: "left", show: false },
    ],
    []
  );

  const getSeriesStyle = useCallback(
    () => ({
      color: "url(#gradient-danger)",
      transition: "all .5s ease",
    }),
    []
  );

  return (
    <div className="chart" style={{ height: 200, overflow: "hidden" }}>
      <Chart
        series={series}
        data={data}
        axes={axes}
        getSeriesStyle={getSeriesStyle}
        tooltip
        renderSVG={() => styleDefs}
      />
    </div>
  );
};
