import { FC, useMemo } from "react";
import { DiseaseShAPI } from "../services/diseaseApi";
import "./countryFlag.scss";

const PlaceholderSVG: FC<any> = ({ className }) => (
  <svg
    className={className}
    viewBox="0 0 250 141"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect width="250" height="141" fill="var(--flag-placeholder-color, #000)" />
  </svg>
);

type CountryFlagProps = Pick<
  DiseaseShAPI.RecentCountryStatsResponse,
  "countryInfo"
> & {
  small?: boolean;
};

export const CountryFlag: FC<CountryFlagProps> = ({ countryInfo, small }) => {
  const classNames = useMemo(
    () => ["country-flag", small && "small"].filter(Boolean).join(" "),
    [small]
  );

  if (!countryInfo?.flag) return <PlaceholderSVG className={classNames} />;
  return (
    <img
      className={classNames}
      src={countryInfo.flag}
      alt={`Flag of ${countryInfo.iso3}`}
    />
  );
};
