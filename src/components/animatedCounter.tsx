import React, { FC, useEffect, useRef } from "react";

export type AnimatedCounterProps = {
  end?: number;
  start?: number;
  duration?: number;
  component?: React.ElementType;
};

export const AnimatedCounter: FC<AnimatedCounterProps> = ({
  component: Component = "span",
  start,
  end,
  duration = 1000,
  ...props
}) => {
  const ref = useRef<any>(null);

  useEffect(() => {
    const { requestAnimationFrame } = window;
    if (Number.isNaN(end)) {
      ref.current.innerHTML = 0;
      return;
    }

    if (!requestAnimationFrame) {
      ref.current.innerHTML = end;
      return;
    }

    let startTimestamp: number;
    const step = (timestamp: number) => {
      if (!startTimestamp) startTimestamp = timestamp;

      const progress = Math.min((timestamp - startTimestamp) / duration, 1);
      const actual = parseInt(ref.current.innerHTML || start, 10) || 0;
      const next = Math.floor(progress * ((end || 0) - actual) + actual);

      ref.current.innerHTML = next;

      if (progress < 1) requestAnimationFrame(step);
    };

    requestAnimationFrame(step);
  }, [end, start, duration, ref]);

  return <Component {...props} ref={ref} />;
};
